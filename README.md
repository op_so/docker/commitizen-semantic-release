<!-- vale off -->
# Commitizen semantic-release
<!-- vale on -->

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=flat)](LICENSE)
[![Pipeline Status](https://gitlab.com/op_so/docker/commitizen-semantic-release/badges/main/pipeline.svg)](https://gitlab.com/op_so/docker/commitizen-semantic-release/pipelines)

A [Node.js](https://nodejs.org/) Docker image with [`commitizen`](https://github.com/commitizen/cz-cli) and [`semantic-release`](https://github.com/semantic-release/semantic-release):

* **lightweight** image based on a debian-slim,
* `multiarch` with support of **amd64** and **arm64**,
* **automatically** updated by comparing software bill of materials (`SBOM`) changes,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* a **software bill of materials (`SBOM`) attestation** added using [`Syft`](https://github.com/anchore/syft),
* available on **Docker Hub** and **Quay.io**.

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/docker/commitizen-semantic-release) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-informational?logo=docker&logoColor=white&style=flat-square)](https://hub.docker.com/r/jfxs/commitizen-semantic-release) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-informational?logo=docker&logoColor=white&style=flat-square)](https://quay.io/repository/ifxs/commitizen-semantic-release) The Quay.io registry.

<!-- vale off -->
## Running Commitizen and semantic-release
<!-- vale on -->

### Sign a commit

Use [task](https://taskfile.dev/) with the git [task template](https://gitlab.com/op_so/task/task-templates):

```shell
task git:commit KEY_PATH|K=<key_full_path> MESSAGE|M=<message> [OPTION|O="<options>"] [PULL|P=<n|N>]
```

or manual:

```shell
# Start a container with the mount point of your private key and your git repository
docker run -d --name commit -v "<absolute_path_private_key:/Private" -v "$(pwd):/data" jfxs/commitizen-semantic-release sleep infinity
# Import your private key
docker exec -it commit /bin/sh -c 'export GPG_TTY=$(tty); gpg --batch --import /private/<private_key>'
# Commit
docker exec -it commit /bin/sh -c 'export GPG_TTY=$(tty); git commit -S --signoff -m "Your commit message"'
# Stop and delete the container
docker stop commit && docker rm commit
```

### `Commitizen` with `cz-conventional-changelog`

Use [task](https://taskfile.dev/) with the git [task template](https://gitlab.com/op_so/task/task-templates):

```shell
task git:commitizen KEY_PATH|K=<key_full_path> [PULL|P=<n|N>]
```

or manual:

```shell
# Start a container with the mount point of your private key and your git repository
docker run -d --name commit -v "<absolute_path_private_key:/Private" -v "$(pwd)/data" jfxs/commitizen-semantic-release sleep infinity
# Import your private key
docker exec -it commit /bin/sh -c 'export GPG_TTY=$(tty); gpg --batch --import /private/<private_key>'
# Commit
docker exec -it commit /bin/sh -c 'export GPG_TTY=$(tty); npx cz --signoff'
# Stop and delete the container
docker stop commit && docker rm commit
```

### `semantic-release`

Example of usage with Gitlab-CI:

```yaml
 ...
gitlab-release:
  image: jfxs/commitizen-semantic-release
  stage: gitlab-release
  script:
    - npx semantic-release
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/commitizen-semantic-release/-/blob/main/Dockerfile) and has:

<!-- vale off -->
--SBOM-TABLE--
<!-- vale on -->

[`Dockerhub` Overview page](https://hub.docker.com/r/jfxs/commitizen-semantic-release) has the details of the last published image.

## Versioning

Docker tag definition:

* the semantic-release version used,
* a dash
* an increment to differentiate build with the same version starting at 001

```text
<semantic-release_version>-<increment>
```

<!-- vale off -->
Example: 23.0.0-001
<!-- vale on -->

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the software bill of materials (`SBOM`) attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

<!-- vale off -->
* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
<!-- vale on -->

## License

<!-- vale off -->
This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
